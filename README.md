# Bernard

Extracts ICS files for birthday events from a vCard addressbook.

    $ bernard.js --abook <contacts> --cbook <dir for ICS files>

File names and UIDs are generated reliably from the contact UID, or if
for some reason not present, from name, plus "bernard" and "birthday"
with the date.  This should ensure global uniqueness while allowing
multiple runs without duplicating events.
