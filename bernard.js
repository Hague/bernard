#!/usr/bin/node

const fs = require('fs')
const path = require('path')
const util = require('util')

const commandLineArgs = require('command-line-args')
const vdata = require('vdata-parser')
const ical_gen = require('ical-generator').default

console.log(ical_gen)

const BDAY = 'BDAY'
const FN = 'FN'
const UID = 'UID'
const VCARD = 'VCARD'
const VEVENT = 'VEVENT'
const VCALENDAR = 'VCALENDAR'
const SEQUENCE = 'SEQUENCE'
const DTSTART = 'DTSTART'

function processCommandLineOrFail() {
    const optionDefinitions = [
      { name: 'abook',
        alias: 'a',
        type: String,
        defaultOption: false,
        defaultValue: '.'},
      { name: 'cbook',
        alias: 'c',
        type: String,
        defaultValue: '.'},
      { name: 'help',
        alias: 'h',
        type: Boolean }
    ]

    const options = commandLineArgs(optionDefinitions)

    if (options.help) {
        console.log('Usage: bernard.js [--abook <file/dir>] [--cbook <dir>]')
        console.log('')
        console.log('    --abook <file/dir> the file or directory containing vCards')
        console.log('    --cbook <dir> the directory to write ICS files to')
        process.exit(options.help ? 0 : -1)
    }

    return options
}


function makeUid(vc) {
    if (vc[UID])
        return vc[UID]
    else
        return (vc[FN].replace(/\W/g, '') + vc[BDAY])
}

function makeFilename(vc, folder) {
    return path.join(folder, makeUid(vc) + "-bernard-birthday.ics")
}

/**
 * given a DTSTART element from vdata-parser, turn it into a date
 *
 * @param dtstart DTSTART object from vdata-parser
 * @return Date object for DTSTART
 */
function getDTStartAsDate(dtstart) {
    const m = /(\d\d\d\d)(\d\d)(\d\d)/.exec(dtstart.value)
    // months from 0!
    const dt = new Date(m[1], m[2] - 1, m[3])
    // hack to fix timezone
    dt.setMinutes(-dt.getTimezoneOffset())
    return dt
}

/**
 * Figure out sequence number for new event
 *
 * @param filename the filename bernard will use
 * @param the date of event being created
 * @return 0 if filename is new, same seq as existing if no change to
 *         bdate, +1 if a change occurred
 */
async function getSequence(filename, bdate) {
    vdata.promFromFile = util.promisify(vdata.fromFile)

    try {
        const data = await vdata.promFromFile(filename)
        if (data[VCALENDAR] && data[VCALENDAR][VEVENT]) {
            const vevent = data[VCALENDAR][VEVENT]
            const curDate = getDTStartAsDate(vevent[DTSTART])
            var seq = parseInt(vevent[SEQUENCE])
            if (bdate.getTime() != curDate.getTime())
                seq += 1
            return seq
        }
    } catch (err) {
        // ignore
    }

    return 0
}

async function processVcard(vc, cbook) {
    const promWriteFile = util.promisify(fs.writeFile)

    if (vc[BDAY]) {
        const ical = ical_gen({ domain: "bernard-birthday" })
        const uid = makeUid(vc, cbook)
        const filename = makeFilename(vc, cbook)

        const dateStr = ((vc[BDAY].hasOwnProperty("value")) ?
                         vc[BDAY].value :
                         vc[BDAY])
        const dateArr = dateStr.split('-')
        // horrible hack to get around timezone shifting to another day
        // (dateArr interpreted as UTC, but output to file in local
        // timezone)
        const bdate = new Date(dateArr)
        bdate.setMinutes(-bdate.getTimezoneOffset())

        const seq = await getSequence(filename, bdate)

        const cal = ical.createEvent({
            start: bdate,
            allDay: true,
            uid: uid,
            sequence: seq,
            repeating: { freq: 'YEARLY' },
            summary: "Birthday " + vc[FN],
        })
        promWriteFile(filename, ical.toString() + '\n')
    }
}


async function scanFile(abook, cbook) {
    vdata.promFromFile = util.promisify(vdata.fromFile)
    const data = await vdata.promFromFile(abook)

    if (data[VCARD]) {
        if (data[VCARD].forEach) {
            data[VCARD].forEach(vc => {
                processVcard(vc, cbook)
            })
        } else {
            processVcard(data[VCARD], cbook)
        }
    } else {
        throw "Card is not a vCard"
    }
}


async function scanDirectory(abook, cbook) {
    const readdir = util.promisify(fs.readdir)

    const files = await readdir(abook)
    files.forEach(file => {
        const fullname = path.join(abook, file)
        scanFile(fullname, cbook).catch(err => {
            // Ignore error messages
        })
    })
}


async function doScan(abook, cbook) {
    const lstat = util.promisify(fs.lstat)

    console.log("Scanning for birthdays...")

    const stats = await lstat(abook)
    if (stats.isFile()) {
        await scanFile(abook, cbook)
    } else if (stats.isDirectory()) {
        await scanDirectory(abook, cbook)
    } else {
        throw abook + ' is not a file or directory'
    }

    console.log("Done.")
}


const options = processCommandLineOrFail()
doScan(options.abook, options.cbook).catch(err => {
    console.error(err.message)
    process.exit(-1)
})



